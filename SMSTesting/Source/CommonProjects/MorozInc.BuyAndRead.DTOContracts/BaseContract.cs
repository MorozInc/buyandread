﻿using System.Runtime.Serialization;

namespace MorozInc.BuyAndRead.DTOContracts
{
    /// <summary>
    /// Базовый контракт
    /// </summary>
    [DataContract]
    public class BaseContract
    {
        /// <summary>
        /// ID
        /// </summary>
        [DataMember]
        public int Id { get; set; }
    }
}
