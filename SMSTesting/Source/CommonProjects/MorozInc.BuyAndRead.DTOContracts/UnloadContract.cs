﻿using System.Runtime.Serialization;

namespace MorozInc.BuyAndRead.DTOContracts
{
    /// <summary>
    /// Выгружаемые данные для пре-сейла
    /// </summary>
    [DataContract]
    public class UnloadContract : BaseContract
    {
        /// <summary>
        /// Наименование книги
        /// </summary>
        [DataMember]
        public string BookName { get; set; }

        /// <summary>
        /// Автор
        /// </summary>
        [DataMember]
        public string AuthorName { get; set; }

        /// <summary>
        /// Год издания
        /// </summary>
        [DataMember]
        public int PublicationYear { get; set; }

        /// <summary>
        /// ISBN код
        /// </summary>
        [DataMember]
        public string IsbnCode { get; set; }

        /// <summary>
        /// Картинка обложки
        /// </summary>
        [DataMember]
        public byte[] PictureCover { get; set; }

        /// <summary>
        /// Стоимость книги
        /// </summary>
        [DataMember]
        public decimal Price { get; set; }

        /// <summary>
        /// Количество экземпляров для распродажи
        /// </summary>
        [DataMember]
        public int PresaleCount { get; set; }
    }
}
