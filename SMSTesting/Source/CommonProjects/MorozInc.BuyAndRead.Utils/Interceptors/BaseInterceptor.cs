﻿using System;
using System.Diagnostics;
using System.Linq;
using Castle.DynamicProxy;
using MorozInc.BuyAndRead.Utils.Logs;

namespace MorozInc.BuyAndRead.Utils.Interceptors
{
    public class BaseInterceptor : IInterceptor
    {
        /// <summary>
        /// Логировщик
        /// </summary>
        protected readonly ILog Log;

        public BaseInterceptor(ILog log)
        {
            Log = log;
        }

        /// <summary>
        /// Перехвадчик вызова метода
        /// </summary>
        /// <param name="invocation">Контекст метода</param>
        public virtual void Intercept(IInvocation invocation)
        {
            var descr = string.Concat(invocation.TargetType.Name, ".", invocation.Method.Name);
            this.Log.Info(">>>{0} ({1})", descr, string.Join(", ", invocation.Arguments.Select(a => (a ?? "").ToString()).ToArray()));

            var timer = new Stopwatch();
            try
            {
                timer.Start();
                invocation.Proceed();
            }
            catch (Exception e)
            {
                this.Log.Error(e, "Ошибка в {0}", descr);
                throw;
            }
            finally
            {
                timer.Stop();
                this.Log.Info("<<<{0} ({1} ms), Result={2}", descr, timer.ElapsedMilliseconds, invocation.ReturnValue);
            }
        }
    }
}
