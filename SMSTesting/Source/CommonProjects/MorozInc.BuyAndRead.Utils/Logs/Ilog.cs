﻿using System;

namespace MorozInc.BuyAndRead.Utils.Logs
{
    public interface ILog
    {
        void Error(string message, params object[] args);
        void Error(Exception e, string message, params object[] args);

        void Warning(string message);
        void Info(string message);
        void Info(string message, params object[] args);
    }
}
