﻿using NLog;

namespace MorozInc.BuyAndRead.Utils.Logs
{
    public class Logger : ILog
    {
        private static readonly NLog.Logger Log = LogManager.GetCurrentClassLogger();

        public void Error(string message, params object[] args)
        {
            Log.Error(message, args);
        }

        public void Error(System.Exception e, string message, params object[] args)
        {
            Log.Error(e, message, args);
        }

        public void Warning(string message)
        {
            Log.Warn(message);
        }

        public void Info(string message)
        {
            Log.Info(message);
        }

        public void Info(string message, params object[] args)
        {
            Log.Info(message, args);
        }
    }
}
