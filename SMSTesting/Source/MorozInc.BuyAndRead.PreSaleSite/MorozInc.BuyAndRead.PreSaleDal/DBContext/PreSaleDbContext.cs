﻿using System.Data.Entity;
using MorozInc.BuyAndRead.PreSaleDal.DBContext.Models;

namespace MorozInc.BuyAndRead.PreSaleDal.DBContext
{
    public class PreSaleDbContext : DbContext
    {
        public PreSaleDbContext() : base("PreSaleDBConnectionString")
        {
            Database.SetInitializer<PreSaleDbContext>(null);
        }

        public DbSet<User> Users { get; set; }
        //public DbSet<BooksCount> BooksCount { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("Users");

            base.OnModelCreating(modelBuilder);
        }
    }
}
