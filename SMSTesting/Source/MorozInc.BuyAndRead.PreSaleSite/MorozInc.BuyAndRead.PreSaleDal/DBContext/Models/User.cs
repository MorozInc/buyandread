﻿namespace MorozInc.BuyAndRead.PreSaleDal.DBContext.Models
{
    public class User
    {
        public int Id { get; set; }

        public string PromoCode { get; set; }
    }
}
