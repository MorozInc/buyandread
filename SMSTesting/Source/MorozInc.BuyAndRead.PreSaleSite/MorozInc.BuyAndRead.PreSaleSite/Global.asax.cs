﻿using System;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using System.Web.Optimization;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using MorozInc.BuyAndRead.DataControllers.Infrastructure;
using MorozInc.BuyAndRead.PreSaleSite.Controllers;

namespace MorozInc.BuyAndRead.PreSaleSite
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AutofacRegistration();
        }

        #region Private

        /// <summary>
        /// Регистрация DI для autofac'а
        /// </summary>
        private void AutofacRegistration()
        {
            var builder = new ContainerBuilder();

            var config = GlobalConfiguration.Configuration;

            builder.RegisterControllers(typeof(Global).Assembly)
                .PropertiesAutowired();

            builder.RegisterType<BookApiController>()
                .PropertiesAutowired()
                .InstancePerRequest();
            builder.RegisterModule<AutofacDataControllersModule>();

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        #endregion Private
    }
}