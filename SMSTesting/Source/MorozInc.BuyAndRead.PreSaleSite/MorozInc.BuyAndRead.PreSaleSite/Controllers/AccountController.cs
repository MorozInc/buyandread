﻿using System.Web.Mvc;
using System.Web.Security;
using MorozInc.BuyAndRead.PreSaleBusinessObjects.InterfacesBO;
using MorozInc.BuyAndRead.ViewModels;

namespace MorozInc.BuyAndRead.PreSaleSite.Controllers
{
    public class AccountController : Controller
    {
        private string _defaultViewName = "Login";
        private IAccountBO _accontBO;

        public AccountController(IAccountBO accontBO)
        {
            _accontBO = accontBO;
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel login)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Error = "Ошибки при заполнении данных.";
                return View(_defaultViewName);
            }

            if (_accontBO.ValidateUser(login.PromoCode))
            {
                FormsAuthentication.SetAuthCookie(login.PromoCode, true);
                Session["promoCode"] = login.PromoCode;
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Error = "Неверный промо-код.";
            return View(_defaultViewName);
        }

        public ActionResult CreatePromoAndLogin()
        {
            var newUser = _accontBO.CreateNewUser();

            if (newUser != null)
            {
                FormsAuthentication.SetAuthCookie(newUser.PromoCode, true);
                Session["promoCode"] = newUser.PromoCode;
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Error = "Не удалось сгенерировать новый промо-код. Попробуйте еще раз.";
            return View(_defaultViewName);
        }

        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}