﻿using System.Collections.Generic;
using System.Web.Http;
using MorozInc.BuyAndRead.DataControllers;
using MorozInc.BuyAndRead.ViewModels;

namespace MorozInc.BuyAndRead.PreSaleSite.Controllers
{
    public class BookApiController : ApiController
    {
        private BooksController _booksController;

        public BookApiController(BooksController booksController)
        {
            _booksController = booksController;
        }

        [HttpGet]
        [Route("bookapi/presalebooks")]
        public IEnumerable<BookForSellViewModel> Get()
        {
            return _booksController.UploadPreSale();
        }

        // GET: api/BookApi/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/BookApi
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/BookApi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/BookApi/5
        public void Delete(int id)
        {
        }
    }
}
