﻿using System.Web.Mvc;

namespace MorozInc.BuyAndRead.PreSaleSite.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}