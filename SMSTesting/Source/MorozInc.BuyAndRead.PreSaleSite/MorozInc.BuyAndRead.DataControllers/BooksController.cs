﻿using System.Collections.Generic;
using MorozInc.BuyAndRead.PreSaleBusinessObjects.InterfacesBO;
using MorozInc.BuyAndRead.ViewModels;

namespace MorozInc.BuyAndRead.DataControllers
{
    public class BooksController
    {
        private IBooksBO _booksBO;

        public BooksController(IBooksBO booksBO)
        {
            _booksBO = booksBO;
        }

        public virtual List<BookForSellViewModel> UploadPreSale()
        {
            var result = new List<BookForSellViewModel>();

            var booksFromService = _booksBO.UploadPreSale();

            foreach (var singleBook in booksFromService)
            {
                result.Add(new BookForSellViewModel
                {
                    AuthorName = singleBook.AuthorName,
                    BookName = singleBook.BookName,
                    Id = singleBook.Id,
                    IsbnCode = singleBook.IsbnCode,
                    Price = singleBook.Price,
                    PresaleCount = singleBook.PresaleCount,
                    PublicationYear = singleBook.PublicationYear,
                    PictureCover = singleBook.PictureCover
                });
            }

            return result;
        }
    }
}
