﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using MorozInc.BuyAndRead.PreSaleBusinessObjects;
using MorozInc.BuyAndRead.PreSaleBusinessObjects.BooksUnloadService;
using MorozInc.BuyAndRead.PreSaleBusinessObjects.InterfacesBO;
using MorozInc.BuyAndRead.PreSaleUtils;
using MorozInc.BuyAndRead.Utils.Interceptors;
using MorozInc.BuyAndRead.Utils.Logs;

namespace MorozInc.BuyAndRead.DataControllers.Infrastructure
{
    public class AutofacDataControllersModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //регистрация общих компонентов
            builder.RegisterType<Logger>()
                .As<ILog>()
                .SingleInstance();

            builder.RegisterType<BaseInterceptor>()
                .AsSelf()
                .SingleInstance();

            //data controllers
            builder.RegisterType<BooksController>()
                .AsSelf()
                .PropertiesAutowired()
                .InstancePerLifetimeScope()
                .EnableClassInterceptors()
                .InterceptedBy(typeof(BaseInterceptor));
            
            //BO
            builder.RegisterType<AccountBO>()
                .As<IAccountBO>()
                .PropertiesAutowired()
                .InstancePerLifetimeScope()
                .EnableClassInterceptors()
                .InterceptedBy(typeof(BaseInterceptor));

            builder.RegisterType<BooksBO>()
                .As<IBooksBO>()
                .PropertiesAutowired()
                .InstancePerLifetimeScope()
                .EnableClassInterceptors()
                .InterceptedBy(typeof(BaseInterceptor));

            //utils
            builder.RegisterType<PromoCodeGenerator>()
                .As<IPromoCodeGenerator>()
                .PropertiesAutowired()
                .InstancePerLifetimeScope();

            base.Load(builder);
        }
    }
}
