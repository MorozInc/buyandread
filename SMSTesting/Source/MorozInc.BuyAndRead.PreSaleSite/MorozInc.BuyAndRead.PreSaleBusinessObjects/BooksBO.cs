﻿using System.Collections.Generic;
using System.Linq;
using MorozInc.BuyAndRead.PreSaleBusinessObjects.BooksUnloadService;
using MorozInc.BuyAndRead.PreSaleBusinessObjects.InterfacesBO;

namespace MorozInc.BuyAndRead.PreSaleBusinessObjects
{
    public class BooksBO : IBooksBO
    {
        public virtual List<UnloadContract> UploadPreSale()
        {
            var unloadBookService = new IntegrationServiceClient();

            return unloadBookService.UnloadPreSaleData().ToList();
        }
    }
}
