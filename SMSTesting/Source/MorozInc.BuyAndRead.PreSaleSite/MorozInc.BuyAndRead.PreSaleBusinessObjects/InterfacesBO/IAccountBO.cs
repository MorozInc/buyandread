﻿using MorozInc.BuyAndRead.PreSaleDal.DBContext.Models;

namespace MorozInc.BuyAndRead.PreSaleBusinessObjects.InterfacesBO
{
    /// <summary>
    /// Интерфейс для работы с пользователями
    /// </summary>
    public interface IAccountBO
    {
        /// <summary>
        /// Валидация по промо-коду
        /// </summary>
        /// <param name="promoCode"></param>
        /// <returns></returns>
        bool ValidateUser(string promoCode);

        /// <summary>
        /// Создание нового пользователя
        /// </summary>
        /// <returns></returns>
        User CreateNewUser();
    }
}
