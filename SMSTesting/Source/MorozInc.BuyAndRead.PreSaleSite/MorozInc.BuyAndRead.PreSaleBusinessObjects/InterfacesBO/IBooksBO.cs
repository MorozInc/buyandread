﻿using System.Collections.Generic;
using MorozInc.BuyAndRead.PreSaleBusinessObjects.BooksUnloadService;

namespace MorozInc.BuyAndRead.PreSaleBusinessObjects.InterfacesBO
{
    public interface IBooksBO
    {
        List<UnloadContract> UploadPreSale();
    }
}
