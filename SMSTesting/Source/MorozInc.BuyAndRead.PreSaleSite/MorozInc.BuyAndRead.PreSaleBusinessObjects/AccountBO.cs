﻿using System.Linq;
using MorozInc.BuyAndRead.PreSaleBusinessObjects.InterfacesBO;
using MorozInc.BuyAndRead.PreSaleDal.DBContext;
using MorozInc.BuyAndRead.PreSaleDal.DBContext.Models;
using MorozInc.BuyAndRead.PreSaleUtils;

namespace MorozInc.BuyAndRead.PreSaleBusinessObjects
{
    public class AccountBO : IAccountBO
    {
        public IPromoCodeGenerator _promoCodeGenerator;

        public AccountBO(IPromoCodeGenerator promoCodeGenerator)
        {
            _promoCodeGenerator = promoCodeGenerator;
        }

        public virtual bool ValidateUser(string promoCode)
        {
            using (var dbContext = new PreSaleDbContext())
            {
                return dbContext.Users.Any(m => m.PromoCode == promoCode);
            }
        }

        public virtual User CreateNewUser()
        {
            using (var dbContext = new PreSaleDbContext())
            {
                var result = new User();

                string newPromo;
                do
                {
                    newPromo = _promoCodeGenerator.GetNewPromoCode();
                } while (dbContext.Users.Any(m => m.PromoCode == newPromo));
                
                result.PromoCode = newPromo;

                dbContext.Users.Add(result);
                dbContext.SaveChanges();

                return result;
            }
        }
    }
}
