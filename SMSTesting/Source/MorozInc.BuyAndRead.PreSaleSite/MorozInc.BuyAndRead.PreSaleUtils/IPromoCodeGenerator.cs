﻿namespace MorozInc.BuyAndRead.PreSaleUtils
{
    public interface IPromoCodeGenerator
    {
        string GetNewPromoCode();
    }
}
