﻿using System;
using System.Linq;

namespace MorozInc.BuyAndRead.PreSaleUtils
{
    public class PromoCodeGenerator : IPromoCodeGenerator
    {
        private const int LengthOfPromo = 10;
        private readonly char[] _keys = "ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890".ToCharArray();

        public string GetNewPromoCode()
        {
            var random = new Random();

            return Enumerable
                .Range(1, LengthOfPromo)
                .Select(k => _keys[random.Next(0, _keys.Length - 1)])
                .Aggregate("", (e, c) => e + c);
        }
    }
}
