﻿using System.ComponentModel.DataAnnotations;

namespace MorozInc.BuyAndRead.ViewModels
{
    /// <summary>
    /// Модель логина
    /// </summary>
    public class LoginViewModel
    {
        [Microsoft.Build.Framework.Required]
        [Display(Name = "Промо-код")]
        public string PromoCode { get; set; }
    }
}