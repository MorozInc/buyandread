﻿using System.ComponentModel.DataAnnotations;

namespace MorozInc.BuyAndRead.ViewModels
{
    /// <summary>
    /// Выгружаемые данные для пре-сейла
    /// </summary>
    public class BookForSellViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование книги
        /// </summary>
        [Display(Name = "Наименование книги")]
        public string BookName { get; set; }

        /// <summary>
        /// Автор
        /// </summary>
        [Display(Name = "Автор")]
        public string AuthorName { get; set; }

        /// <summary>
        /// Год издания
        /// </summary>
        [Display(Name = "Год издания")]
        public int PublicationYear { get; set; }

        /// <summary>
        /// ISBN код
        /// </summary>
        [Display(Name = "ISBN код")]
        public string IsbnCode { get; set; }

        /// <summary>
        /// Картинка обложки
        /// </summary>
        [Display(Name = "Картинка обложки")]
        public byte[] PictureCover { get; set; }

        /// <summary>
        /// Стоимость книги
        /// </summary>
        [Display(Name = "Стоимость книги")]
        public decimal Price { get; set; }

        /// <summary>
        /// Количество экземпляров для распродажи
        /// </summary>
        [Display(Name = "Количество экземпляров")]
        public int PresaleCount { get; set; }
    }
}