﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Autofac.Extras.DynamicProxy;
using MorozInc.BuyAndRead.Controllers;
using MorozInc.BuyAndRead.MainSiteService.Interfaces;
using MorozInc.BuyAndRead.MainSiteService.Processes;
using MorozInc.BuyAndRead.Utils.Interceptors;
using MorozInc.BuyAndRead.Utils.Logs;

namespace MorozInc.BuyAndRead.MainSiteService.Infrastructure
{
    /// <summary>
    /// Регистрируем зависимости
    /// </summary>
    public class AutofacConfigModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //регистрация общих компонентов
            builder.RegisterType<Logger>()
                .As<ILog>()
                .SingleInstance();

            builder.RegisterType<BaseInterceptor>()
                .AsSelf()
                .SingleInstance();

            builder.RegisterType<IntegrationService>()
                .As<IIntegrationService>()
                .InstancePerDependency()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(BaseInterceptor));

            //controllers
            builder.RegisterType<UnloadPresaleController>()
                .AsSelf()
                .PropertiesAutowired()
                .InstancePerLifetimeScope()
                .EnableClassInterceptors()
                .InterceptedBy(typeof(BaseInterceptor));

            base.Load(builder);
        }
    }
}