﻿using System.Collections.Generic;
using System.ServiceModel;
using MorozInc.BuyAndRead.DTOContracts;

namespace MorozInc.BuyAndRead.MainSiteService.Interfaces
{
    /// <summary>
    /// Сервис интеграции с сайтом "Сэйлы!"
    /// </summary>
    [ServiceContract]
    public interface IIntegrationService
    {
        /// <summary>
        /// Выгрузка данных для предпродаж
        /// </summary>
        [OperationContract]
        List<UnloadContract> UnloadPreSaleData();
    }
}
