﻿using System;
using Autofac;
using Autofac.Integration.Wcf;
using MorozInc.BuyAndRead.Controllers.Infrastructure;
using MorozInc.BuyAndRead.MainSiteService.Infrastructure;

namespace MorozInc.BuyAndRead.MainSiteService
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<AutofacConfigModule>();
            builder.RegisterModule<AutofacControllersConfigModule>();

            AutofacHostFactory.Container = builder.Build();
        }
    }
}