﻿using MorozInc.BuyAndRead.Controllers;
using MorozInc.BuyAndRead.MainSiteService.Interfaces;

namespace MorozInc.BuyAndRead.MainSiteService.Processes
{
    public partial class IntegrationService : IIntegrationService
    {
        private UnloadPresaleController _unloadPresaleController;

        public IntegrationService(UnloadPresaleController unloadPresaleController)
        {
            _unloadPresaleController = unloadPresaleController;
        }
    }
}
