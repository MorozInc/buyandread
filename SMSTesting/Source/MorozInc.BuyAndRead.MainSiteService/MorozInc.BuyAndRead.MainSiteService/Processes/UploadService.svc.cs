﻿using System.Collections.Generic;
using MorozInc.BuyAndRead.DTOContracts;

namespace MorozInc.BuyAndRead.MainSiteService.Processes
{
    public partial class IntegrationService
    {
        public List<UnloadContract> UnloadPreSaleData()
        {
            return _unloadPresaleController.UnloadPreSaleData();
        }
    }
}
