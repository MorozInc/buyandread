﻿using System.Collections.Generic;
using MorozInc.BuyAndRead.DTOContracts;

namespace MorozInc.BuyAndRead.IBusinessObjects
{
    /// <summary>
    /// Бизнес-объект для получения данных по пре-сэйлам
    /// </summary>
    public interface IUnloadPresaleBO
    {
        /// <summary>
        /// Выгрузка всех данных пре-сэйла
        /// </summary>
        /// <returns></returns>
        List<UnloadContract> UnloadPreSaleData();
    }
}
