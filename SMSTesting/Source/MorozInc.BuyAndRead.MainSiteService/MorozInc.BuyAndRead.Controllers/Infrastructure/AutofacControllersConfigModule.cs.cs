﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using MorozInc.BuyAndRead.BusinessObjects;
using MorozInc.BuyAndRead.IBusinessObjects;
using MorozInc.BuyAndRead.Utils.Interceptors;

namespace MorozInc.BuyAndRead.Controllers.Infrastructure
{
    public class AutofacControllersConfigModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UnloadPresaleBO>()
                .As<IUnloadPresaleBO>()
                .PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies)
                .EnableClassInterceptors()
                .InterceptedBy(typeof(BaseInterceptor));

            base.Load(builder);
        }
    }
}
