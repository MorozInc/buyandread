﻿using System.Collections.Generic;
using MorozInc.BuyAndRead.DTOContracts;
using MorozInc.BuyAndRead.IBusinessObjects;

namespace MorozInc.BuyAndRead.Controllers
{
    /// <summary>
    /// Контроллер для выгрзуки данных пре-сэйла
    /// </summary>
    public class UnloadPresaleController : BaseController
    {
        private IUnloadPresaleBO _unloadPresaleBO;

        public UnloadPresaleController(IUnloadPresaleBO unloadPresaleBO)
        {
            _unloadPresaleBO = unloadPresaleBO;
        }

        /// <summary>
        /// Выгрузка всех данных пре-сэйла
        /// </summary>
        /// <returns></returns>
        public virtual List<UnloadContract> UnloadPreSaleData()
        {
            return _unloadPresaleBO.UnloadPreSaleData();
        }
    }
}
