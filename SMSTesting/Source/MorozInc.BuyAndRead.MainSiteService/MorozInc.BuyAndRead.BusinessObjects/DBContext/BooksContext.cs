﻿using System.Data.Entity;
using MorozInc.BuyAndRead.BusinessObjects.Models;

namespace MorozInc.BuyAndRead.BusinessObjects.DBContext
{
    public class BooksContext : DbContext
    {
        public BooksContext() : base("MainSiteDBConnectionString")
        {
            Database.SetInitializer<BooksContext>(null);
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<BooksCount> BooksCount { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BooksCount>().ToTable("BooksCount");
            modelBuilder.Entity<Book>().ToTable("Book")
                .HasRequired(m => m.BooksCount).WithOptional();

            base.OnModelCreating(modelBuilder);
        }
    }
}
