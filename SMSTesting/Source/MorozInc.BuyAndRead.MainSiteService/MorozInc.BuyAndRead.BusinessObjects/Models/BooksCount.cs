﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MorozInc.BuyAndRead.BusinessObjects.Models
{
    public class BooksCount
    {
        public int Id { get; set; }

        public int BookId { get; set; }
        public int Count { get; set; }
        public bool IsPresale { get; set; }

        public virtual Book Book { get; set; }
    }
}
