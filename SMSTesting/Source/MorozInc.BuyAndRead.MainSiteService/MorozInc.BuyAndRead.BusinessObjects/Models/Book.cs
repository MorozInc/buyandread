﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MorozInc.BuyAndRead.BusinessObjects.Models
{
    public class Book
    {
        public int Id { get; set; }

        public string BookName { get; set; }
        public string AuthorName { get; set; }
        public int PublicationYear { get; set; }
        public string IsbnCode { get; set; }
        public byte[] PictureCover { get; set; }
        public decimal Price { get; set; }

        public virtual BooksCount BooksCount { get; set; }
    }
}
