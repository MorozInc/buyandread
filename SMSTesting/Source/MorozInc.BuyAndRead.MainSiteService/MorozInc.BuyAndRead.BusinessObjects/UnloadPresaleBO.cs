﻿using System.Collections.Generic;
using System.Linq;
using MorozInc.BuyAndRead.BusinessObjects.DBContext;
using MorozInc.BuyAndRead.DTOContracts;
using MorozInc.BuyAndRead.IBusinessObjects;

namespace MorozInc.BuyAndRead.BusinessObjects
{
    public class UnloadPresaleBO : IUnloadPresaleBO
    {
        public virtual List<UnloadContract> UnloadPreSaleData()
        {
            var result = new List<UnloadContract>();

            using (BooksContext db = new BooksContext())
            {
                // создаем два объекта User
                foreach (var singleBook in db.Books.ToList())
                {
                    var booksCountC = singleBook.BooksCount;
                    if (booksCountC != null && booksCountC.IsPresale)
                        result.Add(new UnloadContract
                        {
                            Id = singleBook.Id,
                            BookName = singleBook.BookName,
                            AuthorName = singleBook.AuthorName,
                            IsbnCode = singleBook.IsbnCode,
                            PresaleCount = booksCountC.Count,
                            Price = singleBook.Price,
                            PublicationYear = singleBook.PublicationYear,
                            PictureCover = singleBook.PictureCover
                        });
                }
            }

            return result;
        }
    }
}
